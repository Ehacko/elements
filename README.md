<link rel="stylesheet" href="Widgets/main.css"/>
<link rel="stylesheet" href="readme.css"/>

# Widgets

## *Widgets parameters* 

<table>

  <tr><td>
    <details>
      <summary><strong>Format css variables(à venir)</strong></summary>
        <ul>
          <li>--W-titleFont: police pour les titre
          <li>--W-fontA: police principale du site
          <li>--W-fontB: police secondaire du site
    </details>

  <tr><td>
    <details>
      <summary><strong>Layout css variables</strong></summary>
        <ul>
          <li>--W-height: definis la hauteur;
          <li>--W-width: definis la largeur;
          <li>--W-gap: definis l'espacement entre les enfants;
          <li>--W-column: definis le nombre de colones;
          <li>--W-row: definis le nombre de ligne;
          <li>--W-align: definis l'alignement vertical du contenu;
          <li>--W-justify: definis l'alignement horizontal du contenu;
          <li>--W-margin: definis la marge exterieur;
          <li>--W-paddingLeft: definis la marge interieur gauche;
          <li>--W-paddingRight: definis la marge interieur droite;
          <li>--W-paddingTop: definis la marge interieur haut;
          <li>--W-paddingBottom: definis la marge interieur bas;
          <li>--W-flow: definis un affichage en ligne ou en colone;
          <li>--W-repeat: difficile à expliquer;
          <!-- <li>--W-type: string; -->
          <!-- <li>--W-Xpos: int; -->
          <!-- <li>--W-Ypos: int; -->
          <!-- <li>--W-Xsize: int; -->
          <!-- <li>--W-Ysize: int; -->
          <li>--W-shader: decimal;
          <li>--W-radius: gere l'arrondissement;
          <li>--W-borderWidth: definis l'epaisseur de la bordure;
          <li>--W-borderStyle: definis le style de la bordure;
          <li>--W-borderColor: definis la couleur de la bordure;
          <li>--W-overflowX: gere le debordement horizontal;
          <li>--W-overflowY: gere le debordement vertical;
    </details>

  <tr><td>
    <details>
    <summary><strong>Color css variables</strong></summary>
    <ul class="red">
      <li>--W-primaryR: definis les bits rouge de la couleur principal;
      <li>--W-secondaryR: definis les bits rouge de la couleur secondaire;
      <li>--W-primaryG: definis les bits vert de la couleur principal;
      <li>--W-secondaryG: definis les bits vert de la couleur secondaire;
      <li>--W-primaryB: definis les bits bleu de la couleur principal;
      <li>--W-secondaryB: definis les bits bleu de la couleur secondaire;
      <li>--W-primaryA: definis l'opacité de la couleur principal;
      <li>--W-secondaryA: definis l'opacité de la couleur principal;
  </details>
</table>

## *Widgets list*

<details>
  <summary><strong>Bannière</strong></summary>
    class: W-banner
    <p type="checkbox" class="W-banner" value="Bannière">
      <span></span>
      <span>
</details>

<details>
  <summary><strong>input</strong></summary>
    class: W-inp <input type="text" class="W-inp"></div>
</details>

<details>
  <summary><strong>Progress bar</strong></summary>
    <li>class: W-bar <span class="W-bar"><span>
    <li>class: W-barT2 <span class="W-barT2"><span>
</details>

<details>
  <summary><strong>Switcher</strong></summary>
    class: W-switch <input type="checkbox" class="W-switch"></div>
</details>

<!-- 
## *Tips*

  Padding:
  
    if not define the variables W-paddingTop and W-will take the value of W-paddingLeft when W-paddingBottom will take the value of W-paddingTop.

    d -->