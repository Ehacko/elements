const
  fs = require('fs'),
  links = [
    {id: "W-banner", name: "Banner"},
    {id: "W-dbd", name: "Breadcrumb"},
    {id: "W-btn", name: "Button"},
    {id: "W-inp", name: "Input"},
    {id: "W-page", name: "Pagination"},
    {id: "W-bar", name: "ProgressBar"},
    {id: "W-switcher", name: "Switcher"},
    {id: "W-box", name: "Box"},
    {id: "W-card", name: "Card"},
    {id: "W-carousel", name: "Carousel"},
    {id: "W-dropdown", name: "Dropdown"},
    {id: "W-form", name: "Form"},
    {id: "W-list", name: "List"},
    {id: "W-modal", name: "Modal"},
  ],
  writed = () => {
    fs.writeFile('index.html', require('pug').renderFile('./views/index.pug', {
      links: links
    }), function (err) {
      if (err) throw err;
      console.log('Saved!');
    });
  };

  require('./PDIG')({path: "widgets/", recursive: true, writed});